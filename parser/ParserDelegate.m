//
//  ParserDelegate.m
//  parser
//
//  Created by Admin on 21.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ParserDelegate.h"

@implementation ParserDelegate
@synthesize done=m_done;
@synthesize titles=m_titles;
@synthesize error=m_error;

//чистка ресурсов
-(void) dealloc {
    [m_error release];
    [m_titles release];
    [super dealloc];
}

//документ начал парситься. Должен
-(void)parserDidStartDocument:(NSXMLParser *)parser {
    m_done=NO;
    m_titles=[NSMutableArray new];
}

//парсинг окончен
-(void)parserDidEndtDocument:(NSXMLParser *)parser{
    m_done=YES;
}

//если произошла ошибка парсинга
-(void)parser:(NSXMLParser *)parser parserErrorOccurred:(NSError *)parserError {
    m_done =YES;
    m_error = [parserError retain];
}

//если произошла ошибка валидации
-(void)parser:(NSXMLParser *)parser validationErrorOccurred:(NSError *) validationError {
    m_done=YES;
    m_error=[validationError retain];
}
/*
//встретили новый элемент
-(void)parser:(NSXMLParser *) parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    //проверяем, нашли ли мы элемент "titile"
    m_isTitle = [[elementName lowercaseString] isEqualToString:@"titile"];
}
*/
@end
