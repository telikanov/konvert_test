//
//  ViewController.m
//  parser
//
//  Created by Admin on 21.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ViewController.h"
#import "ParserDelegate.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize lable1;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    // По идее тут должен быть еще
    //NSAutorelealsePool * pool = [[NSAutoreleasePool allpc]init]
    
    //создали делегат
    ParserDelegate* delegat = [ParserDelegate new];
    
    //adres cbr
    NSURL* cbrURL =
    [NSURL URLWithString:@"http://www.cbr.ru/scripts/XML_daily.asp?date_req=02/03/2002"];
    
    //создаем парсер при помощи URL, назначаем делегат и запускаем
    NSXMLParser* parser =
    [[NSXMLParser alloc] initWithContentsOfURL:cbrURL];
    [parser setDelegate:delegat];
    [parser parse];
    
    //ждем пока идет загрузка парсинга
    while (!delegat.done) {
        sleep(1);
    }
    
    //проверяем была ли ошибка парсинга
    if(delegat.error ==nil){
        //если нет, выводим результат
        NSString *myString =[delegat.titles description];  //кривое конвертирование, посмотрим, что выйдет
        lable1.text=myString;
    }
    //else {
        //number error
      //  lable1.text =delegat.error;
    //}
    
    //clear resours
    //[delegat release];
    //[parser release];
    
    //return 0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

@end
